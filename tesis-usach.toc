\select@language {english}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introducci\'on}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Antecedentes y motivaci\'on}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Descripci\'on del problema}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Estado del arte}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Definición del problema}{4}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Soluci\'on propuesta}{5}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Características de la solución}{5}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Propósitos de la solución}{6}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Objetivos y alcance del proyecto}{7}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Objetivo general}{7}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Objetivos espec\'ificos}{7}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Alcances}{7}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}Metodolog\'ia y herramientas utilizadas}{8}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Metodolog\'ia}{8}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Herramientas de desarrollo}{8}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Ambiente de desarrollo}{9}{subsection.1.5.3}
\contentsline {section}{\numberline {1.6}Organizaci\'on del documento}{10}{section.1.6}
\contentsline {chapter}{\numberline {2}Síntesis de imágenes de radiointerferometría}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Atacama Large Millimeter-Submillimeter Array}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Principios y conceptos de interferometría}{20}{section.2.2}
\contentsline {section}{\numberline {2.3}Métodos existentes de deconvolución}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}CLEAN}{21}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}MEM Mono-frecuencia}{22}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}MEM Multi-frecuencia}{24}{subsection.2.3.3}
\contentsline {chapter}{\numberline {3}GPGPU}{25}{chapter.3}
\contentsline {chapter}{\numberline {4}Pruebas}{26}{chapter.4}
\contentsline {chapter}{\numberline {5}Resultados}{27}{chapter.5}
\contentsline {chapter}{\numberline {6}Conclusiones}{28}{chapter.6}
\contentsline {chapter}{Referencias bibliogr\'aficas}{30}{chapter*.20}
\contentsline {chapter}{Anexos}{30}{section*.21}
\contentsline {chapter}{\numberline {A}Cálculo del gradiente de la función objetivo}{31}{appendix.A}
